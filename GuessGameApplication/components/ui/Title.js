import { StyleSheet, Text, View } from 'react-native'
const Title = ({children}) => {
  return (
       <Text style={styles.title}>{children}</Text>
  )
}
export default Title;

const styles = StyleSheet.create({
    title: {
        fontFamily: 'open-sans-bold',
        borderWidth: 2,
        borderColor: '#fff',
        textAlign: 'center',
        fontSize: 24,
        color:'#fff',
        padding:12,
        maxWidth: '80%',
        width: 300
    }
})