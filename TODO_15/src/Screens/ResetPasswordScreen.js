import React, {useState} from 'react';
import { View, StyleSheet, Text} from 'react-native';
import Button from '../components/Button';
import Header from '../components/Header';
import { Paragraph } from '../components/Paragraph';
import Background from '../components/Background';
import Logo from '../components/Logo';
import TextInput from '../components/TextInput';
import { emailValidation } from '../core/helpers/emailValidation';
import BackButton from '../components/BackButton';
import { sendPasswordResetEmail } from '../api/auth-api';

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed =  async () => {
        const emailError = emailValidation(email.value);
        if(emailError){
            setEmail({...email, error: emailError});
        }
        setLoading(true)
        const response = await sendPasswordResetEmail(email.value)
        if(response.error){
            alert(response.error);
        }
        else{
            navigation.replace('LoginScreen')
            alert("Reset email has been sent successfully!")

        }
        setLoading(false)
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({value: text, error:""})}
                description="You will receive email with password reset link."
            />
            <Button  loading={loading} mode="contained" onPress={onSubmitPressed}>Send Instructions</Button>
        </Background>
    )
}