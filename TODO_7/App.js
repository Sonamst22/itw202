import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.sq1}/>
      <View style={styles.sq2}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignContent: 'center',
    justifyContent: 'center'
  },
  sq1: {
    backgroundColor:"red",
    width: 100,
    height: 100,
    marginLeft: 550,
    marginTop: 200
  },
  sq2: {
    backgroundColor:"blue",
    width: 100,
    height: 100,
    marginLeft: 550
  },
});
