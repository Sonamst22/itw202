import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>The <Text style={styles.bold}>quick brown fox</Text> jumps over the  {'\n'} lazy dog</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 16
  },
  bold:{
    fontWeight: 'bold'
  }
});
