import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Image from URL</Text>
      <Image
        style={{width:100, height:100}}
        source={{uri: 'https://picsum.photos/100/100',}}
      />
      <Text style={{paddingTop:50}}>Image from Local</Text>
      <Image
        style={{width:100, height:100,}}
        source={require('./assets/React_native_Logo.png')}
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
