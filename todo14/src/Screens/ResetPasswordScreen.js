import React, {useState} from 'react';
import { View, StyleSheet, Text} from 'react-native';
import Button from '../components/Button';
import Header from '../components/Header';
import { Paragraph } from '../components/Paragraph';
import Background from '../components/Background';
import Logo from '../components/Logo';
import TextInput from '../components/TextInput';
import { emailValidation } from '../core/helpers/emailValidation';
import BackButton from '../components/BackButton';

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})

    const onSubmitPressed = () => {
        const emailError = emailValidation(email.value);
        if(emailError){
            setEmail({...email, error: emailError});
        }

    }

    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({value: text, error:""})}
                description="You will receive email with password reset link."
            />
            <Button mode="contained" onPress={onSubmitPressed}>Send Instructions</Button>
        </Background>
    )
}